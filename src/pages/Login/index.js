import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import LoginForm from '../../components/Login';
import './style.css';

function LoginPage(props) {

  return (
    <Container>
      <Row>
        <Col />
        <Col >
          <LoginForm setProfile={props.setProfile} setMessage={props.setMessage} />
        </Col>
        <Col />
      </Row>
    </Container>
  )
}

export default LoginPage;