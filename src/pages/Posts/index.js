import { useEffect, useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import { getAPIPostList, getAPIPostScheduleList } from '../../API/posts';
import PostFilters from '../../components/Filters/PostsFilters';
import { showAPIErrorMessage } from '../../components/Navigation/Messages/messages';
import Feed from '../../components/Posts/Feed';
import NewPost from '../../components/Posts/NewPost';


function PostsPage(props) {

    const [posts, setPosts] = useState([])
    const [schedules, setSchedules] = useState([])
    const [feedLoaded, setFeedLoaded] = useState(false)
    const [schedulesLoaded, setSchedulesLoaded] = useState(false)


    // GET POSTS LIST (FEED)

    async function getPosts(pseudo, reported, dateFrom, dateTo, type, language) {
        setFeedLoaded(false);
        getAPIPostList({
            user: pseudo,
            reported: reported,
            dateFrom: dateFrom,
            dateTo: dateTo,
            type: type,
            language: language
        }, props.profile.token)
            .then((response) => {
                setPosts(response.data.data)
                setFeedLoaded(true);
            })
            .catch((error) => {
                showAPIErrorMessage(error, props);
                setFeedLoaded(true);
            });
    }


    // GET SCHEDULED POSTS LIST

    async function getSchedules() {
        setSchedulesLoaded(false);
        getAPIPostScheduleList(props.profile.token)
            .then(function (response) {
                setSchedules(response.data.data)
            })
            .catch((error) => {
                showAPIErrorMessage(error, props);
            });
        setSchedulesLoaded(true);
    }


    // RUN ONCE : INIT POSTS & SCHEDULES

    useEffect(() => {
        // getPosts();
        getSchedules();
        // eslint-disable-next-line
    }, [])


    // RENDERING    

    return (
        <Container>
            <Row className="postPage">
                <Col>
                    <PostFilters getPosts={getPosts} profile={props.profile} setMessage={props.setMessage} />
                    <NewPost schedules={schedules} getSchedules={getSchedules}
                        schedulesLoaded={schedulesLoaded} getPosts={getPosts}
                        profile={props.profile} setMessage={props.setMessage} />
                </Col>
                <Col>
                    <Feed posts={posts} feedLoaded={feedLoaded} />
                </Col>
            </Row>
        </Container>
    )
}

export default PostsPage;