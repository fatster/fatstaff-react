import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import Profile from '../../components/Profile';

function ProfilePage(props) {

    return (
        <Container>
            <Row>
                <Col sm="3" />
                <Col>
                    <Profile profile={props.profile} setProfile={props.setProfile} setMessage={props.setMessage} />
                </Col>
                <Col sm="3" />
            </Row>
        </Container>
    )
}

export default ProfilePage;