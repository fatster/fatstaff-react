import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { getAPIUserDetail, getAPIUserList } from '../../API/users';
import UserFilters from "../../components/Filters/UserFilters";
import { showAPIErrorMessage } from '../../components/Navigation/Messages/messages';
import UserDetail from "../../components/Users/UserDetail";
import UsersList from "../../components/Users/UsersList";

function UsersPage(props) {

    const [selectedUser, setSelectedUser] = useState()
    const [users, setUsers] = useState([])
    const [loaded, setLoaded] = useState(false)


    // LOAD SELECT USER

    const getUserDetail = (userId) => {
        getAPIUserDetail(userId, props.profile.token)
            .then((response) => {
                setSelectedUser(response.data.data)
            })
            .catch((error) => {
                showAPIErrorMessage(error, props);
            });
    }


    // USERS LIST : REFRESH DEPENDING ON CRITERIAS

    const getUsers = (checkedBadges, nbPosts, nbLikes, weightGoal) => {
        getAPIUserList({
            badges: checkedBadges,
            posts_nb: nbPosts,
            likes_nb: nbLikes,
            weight_goal: weightGoal
        }, props.profile.token)
            .then((response) => {
                setUsers(response.data.data)
                setLoaded(true);
            })
            .catch((error) => {
                showAPIErrorMessage(error, props);
            });
    }


    // RUN ONCE : LOAD USERS LIST

    useEffect(() => {
        getUsers()
        // eslint-disable-next-line
    }, [])


    // RENDERING

    return (
        <Container>
            <Row>
                <Col>
                    <UserFilters getUsers={getUsers} setMessage={props.setMessage} />
                    {selectedUser &&
                        <UserDetail user={selectedUser} getUserDetail={getUserDetail} getUsers={getUsers}
                            profile={props.profile} setMessage={props.setMessage} />
                    }
                </Col>
                <Col>
                    <UsersList getUserDetail={getUserDetail} users={users} loaded={loaded} />
                </Col>
            </Row>
        </Container>
    )
}

export default UsersPage;