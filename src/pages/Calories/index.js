import React, { useState } from 'react';
import { Col, Container, Row } from 'react-bootstrap';
import Calories from '../../components/Calories';
import CaloriesFilters from '../../components/Filters/CaloriesFilters';


function CaloriesPage(props) {

    const [pseudo, setPseudo] = useState();


    // RENDERING

    return (
        <Container>
            <Row>
                <Col>
                    <CaloriesFilters profile={props.profile} setMessage={props.setMessage}
                        pseudo={pseudo} setPseudo={setPseudo} />
                </Col>
                <Col>
                    <Calories profile={props.profile} setMessage={props.setMessage}
                        pseudo={pseudo} />
                </Col>
            </Row>
        </Container >
    )
}

export default CaloriesPage;