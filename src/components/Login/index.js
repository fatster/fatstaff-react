import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { useHistory } from 'react-router';
import { postAPIAuth } from '../../API/auth';
import { showAPIErrorMessage } from '../Navigation/Messages/messages';
import './style.css';


function LoginForm(props) {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const history = useHistory();

    const handleEmail = (e) => {
        setEmail(e.target.value)
    }

    const handlePassword = (e) => {
        setPassword(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        postAPIAuth({
            email: email,
            password: password
        })
        .then((response) => {
            props.setProfile({
                user: response.data.user,
                token: response.data.access_token
            });
            props.setMessage(null, ['reset']);
            history.push('/users');
        })
        .catch((error) => {
            showAPIErrorMessage(error, props);
            props.setProfile();
        })
    }

    return (
        <div className="loginComponent">
            <h3 className="loginTitle">Login</h3>
            <form onSubmit={handleSubmit}>
                <div className="loginField">
                    <span className="loginLabel">E-mail</span>
                    <div className="loginFieldInput"><Form.Control type="email" required value={email} onChange={handleEmail} /></div>
                </div>
                <div className="loginField">
                    <span className="loginLabel">Password</span>
                    <div className="loginFieldInput"><Form.Control type="password" required value={password} onChange={handlePassword} /></div>
                </div>
                <div className="loginSubmit"><Button className="loginSubmitButton" type="submit">Submit</Button></div>
            </form>
        </div>
    )
}

export default LoginForm