import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useEffect, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { useParams } from 'react-router';
import { getAPIPostLanguageList, getAPIPostTypeList } from '../../../API/posts';
import { showAPIErrorMessage } from '../../Navigation/Messages/messages';
import './style.css';


function PostFilters(props) {

    const param_pseudo = useParams().pseudo;

    const [typeOptions, setTypeOptions] = useState([])
    const [languageOptions, setLanguageOptions] = useState([])
    const [reported, setReported] = useState(false);
    const [dateFrom, setDateFrom] = useState();
    const [dateTo, setDateTo] = useState();
    const [type, setType] = useState();
    const [pseudo, setPseudo] = useState();
    const [language, setLanguage] = useState();


    // RUN ONCE : INIT POST TYPES LIST

    useEffect(() => {
        getAPIPostTypeList(props.profile.token)
            .then((response) => {
                setTypeOptions(response.data.data)
            })
            .catch((error) => {
                showAPIErrorMessage(error, props);
            });

        getAPIPostLanguageList(props.profile.token)
            .then((response) => {
                setLanguageOptions(response.data.data)
            })
            .catch((error) => {
                showAPIErrorMessage(error, props);
            });
        // eslint-disable-next-line
    }, [])


    // SET PSEUDO FROM PROPS

    useEffect(() => {
        if (param_pseudo) {
            setPseudo(param_pseudo);
        }
        props.getPosts(param_pseudo);
        // eslint-disable-next-line
    }, [param_pseudo])


    // FORM MANAGEMENT

    const handlePseudo = (e) => {
        setPseudo(e.target.value)
    }

    const handleReported = () => {
        setReported(!reported)
    }

    const handleDateFrom = (e) => {
        setDateFrom(e.target.value)
    }

    const handleDateTo = (e) => {
        setDateTo(e.target.value)
    }

    const handleType = (e) => {
        setType(e.target.value)
    }

    const handleLanguage = (e) => {
        setLanguage(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        props.getPosts(pseudo, reported, dateFrom, dateTo, type, language);
    }


    // COSMETICS

    const labelize = (text) => {
        // replace underscore by space
        let newText = text.replace(/_/g, ' ')
        // Capitalize
        newText = newText.charAt(0).toUpperCase() + newText.substring(1).toLowerCase();
        return newText;
    }


    // RENDERING

    return (
        <div className="postFilters">
            <form onSubmit={handleSubmit}>
                <h4 className="titleFilters">Filters</h4>
                <div className="postFilterLign">
                    <span className="postFilterTitle">By author pseudo</span>
                    <span className="wildcard" title="The percent sign (%) represents zero, one, or multiple characters.">(use % as wildcard)</span>
                    <Form.Control type="text" value={pseudo} onChange={handlePseudo} />
                </div>
                <div className="postFilterLign">
                    <span className="postFilterTitle">By reported posts/comments</span>
                    <Form.Check className="checkReported" type="checkbox" onChange={handleReported} />
                </div>
                <div className="postFilterLign">
                    <span className="postFilterTitle">By date</span>
                    <div className="interval">
                        <span className="dateText">Between</span>
                        <Form.Control type="date" onChange={handleDateFrom} />
                        <span className="dateText">and</span>
                        <Form.Control type="date" onChange={handleDateTo} />
                    </div>
                </div>
                <div className="postFilterLign">
                    <span className="postFilterTitle">By post type</span>
                    <Form.Control as="select" onChange={handleType}>
                        <option></option>
                        {typeOptions.map(type => (
                            <option key={type.key} value={type.value} >{labelize(type.key)}</option>
                        ))}
                    </Form.Control>
                </div>
                <div className="postFilterLign">
                    <span className="postFilterTitle">By language/location</span>
                    <Form.Control as="select" onChange={handleLanguage}>
                        <option></option>
                        {languageOptions.map(lang => (
                            <option key={lang.key} value={lang.value} >{labelize(lang.key)}</option>
                        ))}
                    </Form.Control>
                </div>
                <Button variant="success" type="submit">Submit</Button>
            </form>
        </div>
    )
}

export default PostFilters
