import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useEffect, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { useParams } from 'react-router';
import './style.css';


function CaloriesFilters(props) {

    const [pseudo, setPseudo] = useState();

    const param_pseudo = useParams().pseudo;


    // SET PSEUDO FROM PROPS

    useEffect(() => {
        if (param_pseudo) {
            setPseudo(param_pseudo);
            props.setPseudo(param_pseudo);
        } else {
            setPseudo(props.pseudo);
        }
    }, [param_pseudo])


    // FORM MANAGEMENT

    const handlePseudo = (e) => {
        setPseudo(e.target.value)
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        props.setPseudo(pseudo);
    }


    // RENDERING

    return (
        <div className="caloriesFilters">
            <form onSubmit={handleSubmit}>
                <h4 className="caloriesFiltersTitle">Filters</h4>
                <div className="caloriesFiltersLine">
                    <span className="caloriesFiltersLabel">By pseudo</span>
                    <div className="caloriesFiltersInput">
                        <Form.Control type="text" value={pseudo} required onChange={handlePseudo} />
                    </div>
                </div>
                <Button variant="success" type="submit">Submit</Button>
            </form>
        </div>
    )
}

export default CaloriesFilters
