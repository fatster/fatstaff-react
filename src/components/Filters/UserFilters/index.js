import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useEffect, useState } from 'react';
import { Button, DropdownButton, Form } from 'react-bootstrap';
import { getAPIBadgeList } from '../../../API/badges';
import { showAPIErrorMessage } from '../../Navigation/Messages/messages';
import './style.css';


function UserFilters(props) {

    const [options, setOptions] = useState([])
    const [nbPosts, setNbPosts] = useState(undefined)
    const [nbLikes, setNbLikes] = useState(undefined)
    const [weightGoal, setWeightGoal] = useState(undefined)
    const [badges, setBadges] = useState([])


    // RUN ONCE : INIT BADGE LIST

    useEffect(() => {
        getAPIBadgeList(props.token)
            .then((response) => {
                setOptions(response.data.data)
            })
            .catch((error) => {
                showAPIErrorMessage(error, props);
            });
        // eslint-disable-next-line
    }, [])


    // FORM MANAGEMENT

    const handlePostChange = (event) => {
        setNbPosts(event.target.value)
    }

    const handleLikeChange = (event) => {
        setNbLikes(event.target.value)
    }

    const handleWeigthChange = (event) => {
        setWeightGoal(event.target.value)
    }

    const handleBadgeChange = (idBadge) => {

        if (!badges.includes(idBadge)) {
            let add = [...badges, idBadge]
            setBadges(add)
        } else {
            var index = badges.indexOf(idBadge)
            var badgesBis = badges
            badgesBis.splice(index, 1)
        }
    }

    const handleSubmit = (event) => {
        event.preventDefault();
        props.getUsers(badges, nbPosts, nbLikes, weightGoal)
        setNbPosts("")
        setNbLikes("")
        setWeightGoal("")
    }


    // RENDERING

    return (
        <div className="filters">
            <h4 className="titleFilters">Filters</h4>
            <form onSubmit={handleSubmit}>
                <div className="interLign">
                    <span className="infoTitle">By badge</span>
                    <div className= "dropdown">
                        <DropdownButton id="dropdown-basic-button" title="Badges">
                            {options.map(badge => (<div key={badge.id} className="checkBox"><Form.Check label={badge.name_en} type="checkbox" onChange={() => { handleBadgeChange(badge.id) }} /></div>))}
                        </DropdownButton>
                    </div>
                </div>
                <div className="interLign">
                    <span className="infoTitle">By number of posts</span>
                    <span className="indication">(more than)</span>
                    <Form.Control type="number" value={nbPosts} onChange={handlePostChange} />
                </div>
                <div className="interLign">
                    <span className="infoTitle">By number of likes</span>
                    <span className="indication">(more than)</span>
                    <Form.Control type="number" value={nbLikes} onChange={handleLikeChange} />
                </div>
                <div className="interLign">
                    <span className="infoTitle">By weight goal</span>
                    <span className="indication">(more than)</span>
                    <Form.Control type="number" value={weightGoal} onChange={handleWeigthChange} />
                </div>
                <div className="submitUser"><Button className="userFilterSubmit" variant="success" type="submit">Submit</Button></div>
            </form>
        </div>
    )
}

export default UserFilters
