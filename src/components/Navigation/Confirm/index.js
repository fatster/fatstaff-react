import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, Modal } from 'react-bootstrap';
import './style.css';

function Confirm(props) {

    return (
        <>
            <Modal show={props.show} onHide={props.handleDeny}>
                <Modal.Header closeButton>
                    <Modal.Title>Confirmation</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {props.sentences.map((sentence, index) => (
                        <p className="sentence" key={index}>{sentence}</p>
                    ))}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="danger" onClick={props.handleConfirm}>Confirm</Button>
                    <Button variant="secondary" onClick={props.handleDeny}>Cancel</Button>
                </Modal.Footer>
            </Modal>
        </>
    )
}

export default Confirm
