import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { getAPIAuth } from '../../../API/auth';
import { MenuData } from './menu_data';
import './style.css';

function Menu(props) {

    const history = useHistory();

    const location = useLocation();


    // ON LOCATION CHANGE, CHECK IF TOKEN IS STILL VALID

    useEffect(() => {
        if (props.profile && props.profile.token) {
            console.log()
            getAPIAuth(props.profile.token)
                .catch(() => {
                    props.setProfile();
                    props.setMessage({
                        text: 'Token is expired',
                        variant: 'danger'
                    }, ['add']);
                });
        }
    }, [location, props]);


    // ON LOCATION CHANGE, CHECK IF COOKIE IS OK

    // useEffect(() => {
    //     getAPIAuthUser()
    //         .then((response) => {
    //             props.setProfile({
    //                 user: response.user,
    //                 token: response.token
    //             });
    //         }).catch((error) => {
    //             props.setProfile();
    //             props.setMessage({
    //                 text: 'Authentication is rejected',
    //                 variant: 'danger'
    //             }, ['add']);
    //         });
    //     // eslint-disable-next-line
    // }, [location]);


    const logoutHandler = () => {
        props.setMessage(null, ['reset']);
        props.setProfile();
        history.push('/');
    }


    return (
        <div className="navbar">
            <img className="logo" src="/img/fatstaff.png" alt="FatStaff" />
            {props.profile &&
                <div className="menuList" >
                    {MenuData.map((item, index) => {
                        return (
                            <div key={index} className="menu">
                                <Link to={item.path}>
                                    <span className="nav-icon">{item.icon}</span><span className="nav-text"> {item.title}</span>
                                </Link>
                            </div>
                        )
                    })}
                </div>
            }
            {props.profile &&
                <Button variant="dark" className="logout" onClick={logoutHandler}>Logout</Button>
            }
        </div>

    )
}

export default Menu
