import React from 'react'
import * as FaIcons from "react-icons/fa"
import { IoFastFood } from "react-icons/io5";
import { RiAdminLine } from "react-icons/ri";
import { BsFilePost } from "react-icons/bs";


export const MenuData = [
   
    {
        title: 'Users',
        path: '/users',
        icon: <FaIcons.FaUsers/>,
        cName: 'nav-text'
    },
    {
        title: 'Posts',
        path: '/posts',
        icon: <BsFilePost/>,
        cName: 'nav-text'
    },
    {
        title: 'Calories',
        path: '/calories',
        icon: <IoFastFood/>,
        cName: 'nav-text'
    },
    {
        title: 'Profile',
        path: '/profile',
        icon: <RiAdminLine/>,
        cName: 'nav-text'
    },

]