import { Redirect, Route } from "react-router";

function PrivateRoute({ children, profile, ...rest }) {
    return (
        <Route {...rest} render={() => {
            return profile ? children : <Redirect to="/" />
        }} />
    )
}

export default PrivateRoute;