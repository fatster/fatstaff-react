class MessageSequence {
    static sequence = 0;
    static next() {
        return this.sequence++;
    }
}

export default MessageSequence;