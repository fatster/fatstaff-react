import { Col, Row } from "react-bootstrap";
import Message from "./message";
import './style.css';

function Messages(props) {

    return props.messages
        ? (<Row className="messages">
            <Col sm='3' />
            <Col >
                {props.messages.map((message) => {
                    return (
                        <Message key={message.id} message={message} setMessage={props.setMessage} />
                    )
                })}
            </Col>
            <Col sm='3' />
        </Row>)
        : null;

}

export default Messages;


// STANDARD PROCESS FOR API ERRORS

export const showAPIErrorMessage = (error, props) => {
    if (error.response) {
        // CODES 4XX
        if (error.response.status >= 400 && error.response.status < 500) {
            props.setMessage({
                text: error.response.data.message,
                variant: 'danger'
            }, ['reset', 'add'])

            // OTHER ERROR CODES
        } else {
            props.setMessage({
                text: 'A error occured during server processing',
                variant: 'danger',
                stack: [
                    'URL : ' + error.response.request.responseURL,
                    'Server response : ' + error.response.statusText + ' (code ' + error.response.status + ') '
                ]
            }, ['reset', 'add'])
        }

        // OTHER ERROR (NETWORK...)
    } else {
        props.setMessage({
            text: 'A error occured during processing',
            variant: 'danger',
            stack: [
                error.toString()
            ],
        }, ['reset', 'add'])
    }
}
