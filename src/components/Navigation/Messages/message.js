import { useEffect, useState } from "react";
import { Alert } from "react-bootstrap";

function Message(props) {

    const [show, setShow] = useState(true);
    const [counter, setCounter] = useState(5000);
    const [clock, setClock] = useState();


    useEffect(() => {
        let count = 5000;
        let timer = setInterval(() => {
            count -= 1000;
            setCounter(count);
            if (count <= 0) {
                clearInterval(timer);
                handleClose();
            }
        }, 1000);
        setClock(timer);
        // eslint-disable-next-line
    }, []);


    const handleFreeze = () => {
        clearInterval(clock);
        setClock();
    }

    const handleClose = () => {
        props.setMessage(props.message, ['delete']);
        setShow(false);
    }


    return show === true
        ? (<Alert className="messageFrame" variant={props.message.variant} onClick={handleFreeze} onClose={handleClose} dismissible>
            {!props.message.stack
                ? <p>{props.message.text}</p>
                : <Alert.Heading>{props.message.text}</Alert.Heading>}
            {props.message.stack
                ? props.message.stack.map((line, index) => { return <p key={index} className="messageStackLine">{line}</p> })
                : null
            }
            {clock &&
                <div className="messageCounter">({counter / 1000}s)</div>
            }
        </Alert>)
        : null;

}

export default Message;