import React, { useState } from 'react';
import { FaRegCommentDots } from "react-icons/fa";
import { FiHeart } from "react-icons/fi";
import Moment from 'react-moment';
import { useHistory } from 'react-router';
import { getFeedImage, getProfileImage } from '../../../API/image';
import collation_pic from '../../../img/logo.png';
import missing_post_pic from '../../../img/no-image.png';
import missing_profile_pic from '../../../img/no-profile.jpg';
import Comments from '../Comments';
import './style.css';

function Card(props) {

    const history = useHistory();

    const [card, setCard] = useState(false);
    const [show, setShow] = useState(false);

    const post = props.post

    const expandCard = () => setCard(!card);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);


    // LINKS TO PAGES FOR CURRENT USER

    const handleGoCalories = () => {
        history.push('/calories/' + post.user.pseudo);
    }

    const handleGoUsers = () => {
        history.push('/users/' + post.user.id);
    }


    // COSMETICS

    const hoursMinutes = (onlyMinutes) => {
        let hours = Math.floor(onlyMinutes / 60);
        let minutes = onlyMinutes % 60;
        return (hours > 0 ? hours + 'h ' : '') + (minutes > 0 ? minutes + 'm' : '');
    }


    // RENDERING

    return (
        <>
            <div className="cardBody">
                <div className="cardImage" onClick={handleGoCalories} title="Go to Calories">
                    {post.photos[0] &&
                        <img className="cardImg" src={getFeedImage(post.photos[0].path)} alt='postImage'
                            onError={(e) => { if (e.target.src !== missing_post_pic) { e.target.src = missing_post_pic; } }}
                        ></img>
                    }
                    {post.userNutrition &&
                        <div className="cardTimeOfDay">
                            <img className="cardTimeOfDayImg" src={collation_pic} alt='collationImage'></img>
                            <span className="cardTimeOfDayText" >{post.userNutrition.time_of_the_day}</span>
                        </div>
                    }
                    {post.userWeight &&
                        <div className="cardWeight">
                            <span className="cardWeightText">{post.userWeight.weight_difference}&nbsp;kg</span>
                        </div>
                    }
                </div>
                {post.userNutrition &&
                    <div className="cardDetails">
                        {post.userNutrition.nutritionFood.map((nutri, index) => {
                            return (
                                <span key={index} className="ingredient" title={nutri.food.text_en}>{nutri.food.text_en}</span>
                            );
                        })}
                        <span className="calories">{post.userNutrition.grandTotalCalorie}&nbsp;Kcal</span>
                    </div>
                }
                {post.userActivity &&
                    <div className="cardDetails">
                        <span className="ingredient" title={post.userActivity.activity.text_en}>{post.userActivity.activity.text_en}</span>
                        <span className="duration">{hoursMinutes(post.userActivity.duration)}</span>
                        <span className="calories">{post.userActivity.calorie_burned}&nbsp;Kcal&nbsp;&#128293;</span>
                    </div>
                }
                <div className={card ? "cardTextOpen" : "cardText"} onClick={expandCard} >{post.text}</div>
                <div className="cardFoot">
                    <div className="cardUserPhoto" onClick={handleGoUsers} title="Go to Users">
                        <img className="cardUserPhotoImg" src={getProfileImage(post.user.photo_id)} alt="postImage"
                            onError={(e) => { if (e.target.src !== missing_profile_pic) { e.target.src = missing_profile_pic; } }}
                        ></img>
                    </div>
                    <div className="cardUserInfos" onClick={handleGoUsers} title="Go to Users">
                        <div className="cardUserName">{post.user.pseudo}</div>
                        <div className="cardTime"><Moment duration={post.created_at}></Moment> ago</div>
                    </div>
                    <div className="comment_like">
                        <div className="comment"><FaRegCommentDots className="commentIcon" onClick={handleShow} /> {post.comments_count}</div>
                        <div className="like"><FiHeart className="heartIcon" /> {post.likes_count ? post.likes_count : "0"}</div>
                    </div>
                </div>
            </div>
            <Comments show={show} handleClose={handleClose} comments={post.comments} />
        </>
    )
}

export default Card
