import 'bootstrap/dist/css/bootstrap.min.css';
import moment from 'moment';
import * as ReactBootstrap from "react-bootstrap";
import { Table } from 'react-bootstrap';
import './style.css';

function ScheduledPosts(props) {
    return (
        <div className="ScheduledPost">
            <h4 className="titleScheduledPosts">Scheduled posts</h4>
            {props.schedulesLoaded === true &&
                <div className="scheduleTable">
                    <Table striped bordered hover size="sm">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Scheduled</th>
                                <th>Pin end</th>
                            </tr>
                        </thead>
                        <tbody>
                            {props.schedules.map(schedule => (
                                <tr onClick={() => props.scheduleHandler(schedule)} key={schedule.id}>
                                    <td>{schedule.title}</td>
                                    <td>{moment(schedule.post_date).format('MMM D YYYY')}</td>
                                    <td>{schedule.pin_end_date ? moment(schedule.pin_end_date).format('MMM D YYYY') : ''}</td>
                                </tr>))}
                        </tbody>
                    </Table>
                </div>
            }
            {props.schedulesLoaded !== true &&
                <div className="spinner">
                    <ReactBootstrap.Spinner animation="border" variant="primary" />
                </div>
            }
        </div>
    )
}

export default ScheduledPosts
