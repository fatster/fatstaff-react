import { Button, Modal } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './style.css'

function Comments(props) {

    return (
        <>
            <Modal show={props.show} onHide={props.handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Comments</Modal.Title>
                </Modal.Header>
                <Modal.Body>{props.comments.map((comment, key) => (
                    <div key={key} className="commentModal">
                        <div className="pseudoComment">{comment.user ? comment.user.pseudo : "This user no longer exists"}</div>
                        <div>{comment.text}</div>
                    </div>


                ))}</Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={props.handleClose}>
                        Close
          </Button>

                </Modal.Footer>
            </Modal>
        </>
    )
}

export default Comments
