import * as ReactBootstrap from "react-bootstrap";
import Card from '../Card';
import './style.css';

function Feed(props) {

    const nbPosts = props.posts.length

    return (
        <div className="feedFrame">
            <div className="nbPosts">{nbPosts} posts found</div>
            <div className="feed">
                {props.feedLoaded
                    ? props.posts.map(post => (<Card key={post.id} post={post} />))
                    : <ReactBootstrap.Spinner className="spinner" animation="border" variant="primary" />
                }
            </div>

        </div>
    )
}

export default Feed
