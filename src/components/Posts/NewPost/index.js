import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { BiPin } from "react-icons/bi";
import { MdAddAPhoto } from "react-icons/md";
import { getFeedImage } from '../../../API/image';
import { deletePhoto } from '../../../API/photos';
import { addPhoto, deleteAPIPost, postAPIPost, putAPIPost } from '../../../API/posts';
import Confirm from '../../Navigation/Confirm';
import { showAPIErrorMessage } from '../../Navigation/Messages/messages';
import ScheduledPosts from '../ScheduledPosts';
import './style.css';

function NewPost(props) {

    const [postId, setPostId] = useState(undefined);
    const [photoId, setPhotoId] = useState(undefined);
    const [filePreview, setFilePreview] = useState(undefined);
    const [selectedFile, setSelectedFile] = useState(undefined);
    const [scheduleDate, setScheduleDate] = useState(undefined);
    const [title, setTitle] = useState(undefined);
    const [content, setContent] = useState(undefined);
    const [edit, setEdit] = useState(false);
    const [pinEnd, setPinEnd] = useState(undefined);
    const [pin, setPin] = useState(false);
    const [deletePostShow, setDeletePostShow] = useState(false);
    const [deletePostSentences, setDeletePostSentences] = useState([]);

    const imageHandler = (e) => {
        setSelectedFile(e.target.files[0]);
        const reader = new FileReader();
        reader.onload = () => {
            if (reader.readyState === 2) {
                setFilePreview(reader.result)
            }
        }
        reader.readAsDataURL(e.target.files[0])
    }

    const scheduleDateHandler = (e) => {
        setScheduleDate(e.target.value)
    }
    const titleHandler = (e) => {
        setTitle(e.target.value)
    }
    const contentHandler = (e) => {
        setContent(e.target.value)
    }
    const pinEndHandler = (e) => {
        setPinEnd(e.target.value)
    }


    // DISPLAY SCHEDULED POST

    const scheduleHandler = (schedule) => {
        setFilePreview("");
        setEdit(true)
        setPostId(schedule.post.id)
        setTitle(schedule.title);
        setContent(schedule.post.text ? schedule.post.text : '');
        setScheduleDate(schedule.post_date ? schedule.post_date.split('T')[0] : undefined);
        if (schedule.pin_end_date) {
            setPinEnd(schedule.pin_end_date.split('T')[0]);
            setPin(true);
        } else {
            setPinEnd(undefined);
            setPin(false);
        }
        if (schedule.post.photos[0]) {
            setPhotoId(schedule.post.photos[0].id);
            setFilePreview(getFeedImage(schedule.post.photos[0].path));
        }

    }


    // RESET NEW POST

    const resetPost = () => {
        setPostId(undefined);
        setEdit(false);
        setTitle("");
        setContent("");
        setScheduleDate("");
        setPinEnd(undefined);
        setSelectedFile("");
        setFilePreview("");
        setPhotoId("");
        document.getElementById("input").value = "";
    }


    // CREATE OR UPDATE SCHEDULED POST

    const submitPostHandler = (event) => {
        event.preventDefault();

        //Uploading photo
        let path = null;
        if (selectedFile) {

            const formData = new FormData();
            formData.append("file", selectedFile);

            addPhoto(formData, props.profile.token)
                .then((response) => {
                    path = response.data.data.photoId;
                    console.log(path)
                })
                .then(() => {
                    addOrUpdatePost(path);
                })
        } else {
            addOrUpdatePost();
        }

    }

    const addOrUpdatePost = (path) => {
        const data = {
            text: content,
            path: path,
            scheduled: scheduleDate|| pinEnd ? {
                title: title,
                post_date: scheduleDate,
                pin_end_date: pinEnd,
            } : undefined
        };

        const api = edit ? putAPIPost(postId, data, props.profile.token) : postAPIPost(data, props.profile.token);
        api.then(() => {
            if (scheduleDate) {
                props.getSchedules();
            } else {
                props.getPosts();
            }
            resetPost();
            cancelPin();
        });
    }


    // DELETE EDITED POST

    const handleDeletePost = () => {
        setDeletePostSentences([
            'Are you sure you want to delete this scheduled post ?',
            '\u21E8 title : ' + title
        ])
        setDeletePostShow(true);
    }

    const handleDeletePostDeny = () => setDeletePostShow(false);

    const handleDeletePostConfirm = () => {
        deleteAPIPost(postId, props.profile.token)
            .then(() => {
                setDeletePostShow(false);
                props.setMessage({
                    text: "Scheduled post '" + title + "' has been deleted",
                    variant: 'success'
                }, ['reset', 'add'])
                props.getSchedules();
                resetPost();
                cancelPin();
            })
            .catch((error) => {
                setDeletePostShow(false);
                showAPIErrorMessage(error, props);
            });
    }


    // DELETE POST IMAGE

    const deleteImage = () => {
        if (edit && photoId) {
            deletePhoto(photoId, props.profile.token)
            setFilePreview((""))
            setSelectedFile((""))
            document.getElementById("input").value = "";
        } else {
            setFilePreview("")
            setSelectedFile("")
            document.getElementById("input").value = "";
        
        }

    }


    // PIN MANAGEMENT

    const displayPin = () => {
        setPin(!pin)
    }

    const cancelPin = () => {
        setPinEnd(undefined);
        setPin(false)
    }


    // RENDERING

    return (
        <div className="newPost">
            <div className="blockTitlePost">
                <h4 className="titleNewPost">{edit ? "Edit Post" : "New Post"}</h4>
                {edit && <Button className="cancelEdit" variant="outline-danger" onClick={resetPost}>x</Button>}
            </div>
            <form>
                <div className="postTitle">
                    <Form.Control className="newPostInput" type="text" placeholder="Schedule title" onChange={titleHandler} value={title} />
                    <input type="file" accept="image/*" id="input" onChange={imageHandler} />
                    <BiPin className="pinIcon" onClick={displayPin} />
                    <label htmlFor="input"><MdAddAPhoto className="photoIcon" /></label>

                </div>

                <div className="postContent">
                    <Form.Control className="newPostText" as="textarea" rows={3} placeholder="Post content" onChange={contentHandler} value={content} />
                    <div className="postImage">
                        {filePreview && <Button className="deleteImg" variant="danger" onClick={deleteImage}>x</Button>}
                        {filePreview && <img className="postImg" src={filePreview} alt="uploadedImage" />}
                    </div>
                </div>

                <div className="postButtons">
                    <span className="postText">Post date </span>
                    <Form.Control type="date" onChange={scheduleDateHandler} value={scheduleDate} />
                    {!scheduleDate && <span className="or">Or </span>}
                    {scheduleDate && <Button className="cancelPin" variant="outline-danger" onClick={() => { setScheduleDate("") }}>x</Button>}
                    <Button variant={scheduleDate ? "primary" : "success"} type="submit" onClick={submitPostHandler}>{scheduleDate ? "Schedule it" : "Submit now"} </Button>
                    {edit && <Button variant="danger" type="button" onClick={handleDeletePost}>Delete</Button>}
                    <Confirm sentences={deletePostSentences} show={deletePostShow} handleDeny={handleDeletePostDeny} handleConfirm={handleDeletePostConfirm} />
                </div>
                {pin &&
                    <div className="pinPost">
                        <span className="postText"> Pin post until </span>
                        <Form.Control type="date" onChange={pinEndHandler} value={pinEnd} />
                        {pinEnd && <Button className="cancelPin" variant="outline-danger" onClick={cancelPin}>x</Button>}
                    </div>}

            </form>
            <ScheduledPosts schedules={props.schedules}
                scheduleHandler={scheduleHandler}
                schedulesLoaded={props.schedulesLoaded} />
        </div>
    )


}

export default NewPost
