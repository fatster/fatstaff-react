import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState } from 'react';
import { Button, Col, Form, Row } from 'react-bootstrap';
import { getProfileImage } from '../../API/image';
import { putAPIProfile, updatePhoto } from '../../API/users';
import missing_photo from '../../img/no-profile.jpg';
import { showAPIErrorMessage } from '../Navigation/Messages/messages';
import './style.css';


function Profile(props) {

    const [pseudo, setPseudo] = useState();
    const [firstName, setFirstName] = useState();
    const [lastName, setLastName] = useState();
    const [email, setEmail] = useState();
    const [password, setPassword] = useState();
    const [newPassword, setNewPassword] = useState();
    const [confirmPassword, setConfirmPassword] = useState();
    const [photo, setPhoto] = useState();

    // RUN ON PROFILE CHANGE : INIT FORM

    useEffect(() => {
        setPseudo(props.profile.user.pseudo);
        setFirstName(props.profile.user.name);
        setLastName(props.profile.user.surname);
        setEmail(props.profile.user.email);
        setPassword('');
        setNewPassword('');
        setConfirmPassword('');
        // eslint-disable-next-line
    }, [])


    // FORM MANAGEMENT

    const handlePseudo = (e) => {
        setPseudo(e.target.value);
    }
    const handleFirstName = (e) => {
        setFirstName(e.target.value);
    }
    const handleLastName = (e) => {
        setLastName(e.target.value);
    }
    const handleEmail = (e) => {
        setEmail(e.target.value);
    }
    const handlePassword = (e) => {
        setPassword(e.target.value);
    }
    const handleNewPassword = (e) => {
        setNewPassword(e.target.value);
    }
    const handleConfirmPassword = (e) => {
        setConfirmPassword(e.target.value);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        if (validateForm() === true) {

            if (photo) {
                let photoId = null;
                const formData = new FormData();
                formData.append("file", photo);

                updatePhoto(formData, props.profile.token)
                    .then((response) => {
                        photoId = response.data.data.photoId
                    })
                    .then(() => {
                        updateProfile(photoId);
                    })
            } else {
                updateProfile();
            }
        }
    }

    const updateProfile = (photoId) => {

        let newProfileDTO = {
            pseudo: pseudo,
            name: firstName,
            surname: lastName,
            email: email,
            password: password,
            newpassword: newPassword,
            photoId: photoId
        };

        putAPIProfile(props.profile.user.id, newProfileDTO, props.profile.token)
            .then((response) => {
                props.setMessage({
                    text: 'Profile has been successfully updated',
                    variant: 'success'
                }, ['reset', 'add']);
                props.setProfile({
                    user: response.data.data,
                    token: props.profile.token
                });
            })
            .catch((error) => {
                showAPIErrorMessage(error, props);
            });
    }

    const validateForm = () => {
        let r = true;
        if (newPassword && confirmPassword && newPassword !== confirmPassword) {
            props.setMessage({
                text: 'New password and Confirm password must be identical',
                variant: 'danger'
            }, ['reset', 'add'])
            r = false;
        }
        return r;
    }

    const handlePhoto = (e) => {
        setPhoto(e.target.files[0])
    }


    // RENDERING

    return (
        <div className="profile">
            <Row>
                <Col md="auto">
                    <img className="userPhoto" src={getProfileImage(props.profile.user.photo_path)} alt="photo_profil"
                        onError={(e) => { if (e.target.src !== missing_photo) { e.target.src = missing_photo; } }}
                    ></img>
                </Col>
                <Col id="pseudo">
                    <div className="userPseudo">{props.profile.user.pseudo}</div>
                </Col>
            </Row>

            <h3 className="informations">User Informations</h3>

            <div className="details" >
                <form onSubmit={handleSubmit}>

                    <div className="detailsRow required">
                        <span className="label">Pseudo</span>
                        <div className="inputText">
                            <Form.Control type="text" required value={pseudo} onChange={handlePseudo} />
                        </div>
                    </div>
                    <div className="detailsRow">
                        <span className="label">First name</span>
                        <div className="inputText">
                            <Form.Control type="text" value={firstName} onChange={handleFirstName} />
                        </div>
                    </div>
                    <div className="detailsRow">
                        <span className="label">Last name</span>
                        <div className="inputText">
                            <Form.Control type="text" value={lastName} onChange={handleLastName} />
                        </div>
                    </div>
                    <div className="detailsRow required">
                        <span className="label">E-mail</span>
                        <div className="inputText">
                            <Form.Control type="text" required value={email} onChange={handleEmail} />
                        </div>
                    </div>
                    <div className="detailsRow">
                        <span className="label">Photo</span>
                        <div className="inputText">
                            <Form.File id="formFile" accept="image/*" onChange={handlePhoto} />
                        </div>
                    </div>
                    <div className="detailsRow required">
                        <span className="label">Password</span>
                        <div className="inputText">
                            <Form.Control type="password" required value={password} onChange={handlePassword} />
                        </div>
                    </div>
                    <div className="detailsRow">
                        <span className="label">New password</span>
                        <div className="inputText">
                            <Form.Control type="password" value={newPassword} onChange={handleNewPassword} />
                        </div>
                    </div>
                    {newPassword && newPassword.length > 0 &&
                        <div className="detailsRow required" >
                            <span className="label">Confirm password</span>
                            <div className="inputText">
                                <Form.Control type="password" value={confirmPassword} onChange={handleConfirmPassword} />
                            </div>
                        </div>
                    }

                    <div className="submitFooter">
                        <Button className="submitButton" variant="success" type="submit">Submit</Button>
                    </div>

                </form>
            </div>

        </div>

    )


}

export default Profile;