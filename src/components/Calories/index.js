import 'bootstrap/dist/css/bootstrap.min.css';
import moment from 'moment';
import { useEffect, useState } from 'react';
import { Button, Col, Dropdown, DropdownButton, Row, Spinner, Table } from 'react-bootstrap';
import { getAPICalories } from '../../API/users';
import { showAPIErrorMessage } from '../Navigation/Messages/messages';
import './style.css';


function Calories(props) {

    const [caloriesLoading, setCaloriesLoading] = useState(false);
    const [calories, setCalories] = useState([]);
    const [monthsNames, setMonthsNames] = useState();
    const [selectedMonthName, setSelectedMonthName] = useState();
    const [weeks, setWeeks] = useState();
    const [selectedWeekNumber, setSelectedWeekNumber] = useState();
    const [days, setDays] = useState();


    // LOAD CALORIES ON PSEUDO CHANGE

    useEffect(() => {
        if (props.pseudo) {
            setCaloriesLoading(true);
            getAPICalories(props.pseudo, props.profile.token)
                .then((response) => {
                    setCalories(response.data.data);
                    setCaloriesLoading(false);
                })
                .catch((error) => {
                    setCalories([]);
                    setCaloriesLoading(false);
                    processErrors(error);
                })
        }
        // eslint-disable-next-line
    }, [props.pseudo]);

    const processErrors = error => {
        if( error.response && error.response.data && error.response.data.message ) {
            if(error.response.data.message.startsWith("TypeError: Cannot read property 'weight' of undefined")) {
                props.setMessage({
                    text: "No weight data found for this user",
                    variant: 'danger'
                }, ['reset', 'add'])
            }
        } else {
            showAPIErrorMessage(error, props);
        }
    }

    useEffect(() => {
        if (calories.history) {
            setSelectedMonthName(); // reset selection
            setMonthsNames(Object.keys(calories.history));
        }
        // eslint-disable-next-line
    }, [calories]);

    useEffect(() => {
        if (monthsNames) {
            setSelectedMonthName(monthsNames[0]);
        }
        // eslint-disable-next-line
    }, [monthsNames]);

    useEffect(() => {
        if (selectedMonthName) {
            let _rawWeeks = calories.history[selectedMonthName];
            let _weeks = [];
            for (let i = 0; i < _rawWeeks.length; i++) {
                if (_rawWeeks[i] !== null) {
                    _weeks.push(_rawWeeks[i]);
                }
            }
            setSelectedWeekNumber(); // reset selection
            setWeeks(_weeks);
        }
        // eslint-disable-next-line
    }, [selectedMonthName]);

    useEffect(() => {
        if (weeks) {
            setSelectedWeekNumber(0);
        }
        // eslint-disable-next-line
    }, [weeks]);

    useEffect(() => {
        if (selectedWeekNumber !== undefined) {
            setDays(weeks[selectedWeekNumber]);
        }
        // eslint-disable-next-line
    }, [selectedWeekNumber]);


    // EVENTS MANAGEMENT

    const handleSelectMonthName = (e) => {
        setSelectedMonthName(monthsNames[e]);
    }

    const handleSelectWeekNumber = (e) => {
        setSelectedWeekNumber(e.target.value);
    }

    // COSMETICS

    const labelizeMonth = (text) => {
        return text.replace(/-/g, ' ')
    }

    const sumWeekCalories = (weekNum) => {
        let _sum = 0;
        for (let i = 0; i < weeks[weekNum].length; i++) {
            _sum += weeks[weekNum][i].calorie;
        }
        return _sum;
    }

    // RENDERING

    return (
        <Table className="caloriesHeader">
            <Row><Col className="caloriesTitle">Calories counter</Col></Row>
            <Row><Col className="caloriesSubtitle">Today</Col></Row>
            {caloriesLoading === true &&
                <Row><Col className="caloriesSpinnerRow">
                    <Spinner className="caloriesSpinner" animation="border" variant="primary" />
                </Col></Row>
            }
            {caloriesLoading === false && !calories.calorie &&
                <Row><Col className="caloriesNoData">No data.</Col></Row>
            }
            {caloriesLoading === false && calories.calorie &&
                <Row>
                    <Col className="caloriesSummaryRow"><Table className="caloriesSummary">
                        <Row>
                            <Col className="caloriesCounter caloriesToday">{calories.calorie.today}</Col>
                            <Col className="caloriesCounter caloriesLoose">{calories.calorie.loose}</Col>
                            <Col className="caloriesCounter caloriesMaintain">{calories.calorie.maintain}</Col>
                        </Row>
                        <Row>
                            <Col className="caloriesLabel caloriesToday">My Intake</Col>
                            <Col className="caloriesLabel caloriesLoose">Weight Loss</Col>
                            <Col className="caloriesLabel caloriesMaintain">Maintain</Col>
                        </Row>
                    </Table></Col>
                </Row>
            }
            {caloriesLoading === false && monthsNames && selectedMonthName &&
                <Row>
                    <Col className="caloriesMonthsRow">
                        <DropdownButton className="caloriesMonthsButton" variant="secondary" size="lg"
                            title={labelizeMonth(selectedMonthName)} onSelect={handleSelectMonthName} >
                            {monthsNames.map((monthName, index) => {
                                return (
                                    <Dropdown.Item key={index} eventKey={index}>{labelizeMonth(monthName)}</Dropdown.Item>
                                );
                            })}
                        </DropdownButton>
                    </Col>
                </Row>
            }
            {caloriesLoading === false && weeks &&
                <Row>
                    <Col className="caloriesWeeksRow">
                        <span className="caloriesWeekButtons">
                            {weeks.map((week, index) => {
                                return (
                                    <Button className="caloriesWeekButton" key={index}
                                        variant={'week' + (index % 4)}
                                        onClick={handleSelectWeekNumber}
                                        value={index}>Week {index + 1}<br />{sumWeekCalories(index)}<br />kcal</Button>
                                );
                            })}
                        </span>
                    </Col>
                </Row>
            }
            {caloriesLoading === false && days &&
                <Row>
                    <Col className="caloriesDaysRow">
                        <Table className="caloriesDaysTable">
                            <Row><Col className="caloriesDaysTitle">Week {parseInt(selectedWeekNumber) + 1}</Col></Row>
                            {days.map((day,index) => {
                                return (
                                    <Row className={day.calorie > 0 ? 'caloriesDayHi' : ''} key={index}>
                                        <Col>{moment(day.date).format('dddd D MMMM')}</Col>
                                        <Col className="caloriesDaysQty">{day.calorie}&nbsp;kcal</Col>
                                    </Row>
                                );
                            })}
                        </Table>
                    </Col>
                </Row>
            }
        </Table >
    )

}

export default Calories;