import { useEffect, useState } from 'react';
import * as ReactBootstrap from "react-bootstrap";
import { useParams } from 'react-router';
import { getProfileImage } from '../../../API/image';
import missing_photo from '../../../img/no-profile.jpg';
import './style.css';


function UsersList(props) {

  const [searchTerm, setSearchTerm] = useState("")

  const nbUsers = props.users.length

  const param_id = useParams().id;


  // SET PSEUDO FROM PROPS

  useEffect(() => {
      if (param_id) {
        props.getUserDetail(param_id)
      }
  }, [param_id, props])


  // RENDERING

  return (
    <>
      <ul className="list">
        <input className="searchUser" type="text" value={searchTerm} placeholder="Search..." onChange={(event) => { setSearchTerm(event.target.value) }} />
        <div className="nbUsers">{nbUsers} users found</div>
        {props.loaded ? props.users.filter((val) => {
          if (searchTerm === "") {
            return val
          } else if (val.pseudo.toLowerCase().includes(searchTerm.toLowerCase())) {
            return val
          } else {
            return undefined
          }
        }).map((user) => {
          return (<li className="element" key={user.id} onClick={() => props.getUserDetail(user.id)}>
            <img className="photo" src={getProfileImage(user.photo_id)} alt=''
              onError={(e) => { if (e.target.src !== missing_photo) { e.target.src = missing_photo; } }}
            ></img>{user.pseudo}</li>)
        }) : <ReactBootstrap.Spinner id="spinner" animation="border" variant="primary" />}
      </ul>
    </>
  );
}

export default UsersList;
