import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState } from 'react';
import { Button, Col, DropdownButton, Form, Row } from 'react-bootstrap';
import { useHistory } from 'react-router';
import Switch from 'react-switch';
import { getAPIBadgeList } from '../../../API/badges';
import { getProfileImage } from '../../../API/image';
import { ADMIN_USER_ROLE, deleteAPIUser, putAPIUser } from '../../../API/users';
import missing_photo from '../../../img/no-profile.jpg';
import Confirm from '../../Navigation/Confirm';
import { showAPIErrorMessage } from '../../Navigation/Messages/messages';
import './style.css';

function UserDetail(props) {

    const history = useHistory();

    const [options, setOptions] = useState([])
    const [deleteUserShow, setDeleteUserShow] = useState(false);
    const [deleteUserSentences, setDeleteUserSentences] = useState([]);

    var ids = []
    var roles = []


    // INIT LOCAL VARIABLES FROM PROPS

    if (props.user.badges) {
        const userBadges = props.user.badges
        userBadges.map(badge => { return (ids = [...ids, badge.id]) })
    }

    if (props.user.roles) {
        const userRoles = props.user.roles
        userRoles.map(role => { return (roles = [...roles, role]) })
    }


    // DISPLAY ONCE : LOAD ALL BADGES

    useEffect(() => {
        getAPIBadgeList(props.profile.token)
            .then((response) => {
                setOptions(response.data.data)
            })
            .catch((error) => {
                showAPIErrorMessage(error, props);
            });
        // eslint-disable-next-line
    }, [])


    // HTML COMPONENTS STATUS

    function isChecked(id) {
        if (props.user.id) {
            if (ids.includes(id)) {
                return true
            } else {
                return false
            }
        }
    }

    function isAdmin() {
        return roles.includes(ADMIN_USER_ROLE);
    }


    // SYNCHRONIZE USER WITH BADGE CHECKBOXES

    const manageBadge = (boolean, id) => {
        if (boolean === true) {
            var index = ids.indexOf(id)
            ids.splice(index, 1)
        } else {
            ids = [...ids, id]
        }
        putAPIUser(props.user.id, { "badges": ids }, props.profile.token)
            .then(function () {
                props.getUserDetail(props.user.id)
            })
            .catch((error) => {
                showAPIErrorMessage(error, props);
            });
    }


    // SYNCHRONIZE USER WITH ADMIN SWITCH

    const manageAdmin = (boolean) => {
        if (boolean === true) {
            var index = roles.indexOf(ADMIN_USER_ROLE)
            roles.splice(index, 1)
        } else {
            roles = [...roles, ADMIN_USER_ROLE]
        }
        putAPIUser(props.user.id, { "roles": roles }, props.profile.token)
            .then(() => {
                props.getUserDetail(props.user.id)
            })
            .catch((error) => {
                showAPIErrorMessage(error, props);
            });
    }


    // LINKS TO PAGES FOR CURRENT USER

    const handleGoPosts = () => {
        history.push('/posts/' + props.user.pseudo);
    }

    const handleGoCalories = () => {
        history.push('/calories/' + props.user.pseudo);
    }


    // DELETE SELECTED USER

    const handleDeleteUser = () => {
        setDeleteUserSentences([
            'Are you sure you want to delete this user ?',
            '\u21E8 pseudo : ' + props.user.pseudo
        ])
        setDeleteUserShow(true);
    }

    const handleDeleteUserDeny = () => setDeleteUserShow(false);

    const handleDeleteUserConfirm = () => {
        deleteAPIUser(props.user.id, props.profile.token, props.profile.user.pseudo)
            .then(() => {
                setDeleteUserShow(false);
                props.setMessage({
                    text: "User '" + props.user.pseudo + "' has been deleted",
                    variant: 'success'
                }, ['reset', 'add'])
                props.getUsers();
            })
            .catch((error) => {
                setDeleteUserShow(false);
                showAPIErrorMessage(error, props);
            })
    }


    // RENDERING

    return (
        <div className="detail">
            <Row>
                <Col md="auto">
                    <img className="photoDetail" src={getProfileImage(props.user.photo_path)} alt="photo_profil"
                        onError={(e) => { if (e.target.src !== missing_photo) { e.target.src = missing_photo; } }}
                    ></img>
                </Col>
                <Col id="pseudoTitle">
                    <div className="name">{props.user.pseudo}</div>
                    {props.user.id &&
                        <div>
                            <span className="statUser">Posts : </span> {props.user.posts_nb}
                            <span className="statUser2">Likes: </span>{props.user.likes_nb}
                        </div>}
                </Col>
            </Row>

            <h4 className="information">Informations</h4>
            <div className="details" >

                <div className="interLign"><span className="infoTitle">First name</span> <span className="infos">{props.user.name ? props.user.name : "not specified"}</span> </div>
                <div className="interLign"><span className="infoTitle"> Last name </span><span className="infos">{props.user.surname ? props.user.surname : "not specified"}</span> </div>
                <div className="interLign"><span className="infoTitle"> E-mail </span><span className="infos">{props.user.email ? props.user.email : "not specified"}</span> </div>
                <div className="interLign"><span className="infoTitle"> Weight goal </span><span className="infos">{props.user.weight_goal} {props.user.id && "kg"}</span> </div>
                <div className="interLign">
                    <span className="infoTitle">Badge</span>

                    <DropdownButton id="dropdown-basic-button" title="Badges">
                        {options.map(badge => (<div key={badge.id} className="checkBox"><Form.Check label={badge.name_en} type="checkbox" onChange={() => { manageBadge(isChecked(badge.id), badge.id) }} checked={isChecked(badge.id)} /></div>))}
                    </DropdownButton>

                </div>
                <div className="interLign">
                    <span className="infoTitle">Admin</span>
                    <span className="switch">
                        <Switch checked={isAdmin()} onChange={() => { manageAdmin(isAdmin()) }} />
                    </span>
                </div>

                <div className="userDetailButtons">
                    <div className="goButton"><Button variant="outline-secondary" onClick={handleGoPosts}>Posts</Button></div>
                    <div className="goButton"><Button variant="outline-secondary" onClick={handleGoCalories}>Calories</Button></div>
                    <div className="deleteUser"><Button variant="danger" onClick={handleDeleteUser}>Delete user</Button></div>
                </div>
                <Confirm sentences={deleteUserSentences} show={deleteUserShow} handleDeny={handleDeleteUserDeny} handleConfirm={handleDeleteUserConfirm} />

            </div>

        </div >

    )
}

export default UserDetail;