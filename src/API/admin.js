// export const BASE_URL = 'http://localhost:3000';
export const BASE_URL = process.env.REACT_APP_BACK_ROOT_URL;

export const ADMIN_BASE_URL = BASE_URL + '/admin';

export const getOptions = (token, params) => {
    return params
        ? { headers: { 'Authorization': `Bearer ${token}` }, params: params }
        : { headers: { 'Authorization': `Bearer ${token}` } };
}
