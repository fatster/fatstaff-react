import axios from "axios";
import { ADMIN_BASE_URL, getOptions } from './admin';

export const ADMIN_USERS_BASE_URL = ADMIN_BASE_URL + '/users';

export const ADMIN_USER_ROLE = "ADMIN";


// ENUM LISTS

export const getAPIUserList = (params, token) => {
    return axios.get(ADMIN_USERS_BASE_URL, getOptions(token, params));
}


// CRUD USER

export const getAPIUserDetail = (id, token) => {
    return axios.get(ADMIN_USERS_BASE_URL + '/' + id, getOptions(token));
}

export const putAPIUser = (id, body, token) => {
    return axios.put(ADMIN_USERS_BASE_URL + '/' + id, body, getOptions(token));
}

export const deleteAPIUser = (id, token, admin_pseudo) => {
    return axios.delete(ADMIN_USERS_BASE_URL,
        getOptions(token, { id: id, admin_pseudo: admin_pseudo }));
}


// UPDATE PROFILE

export const putAPIProfile = (id, body, token) => {
    return axios.put(ADMIN_USERS_BASE_URL + '/profile/' + id, body, getOptions(token));
}


// GET CALORIES PER USER

export const getAPICalories = (pseudo, token) => {
    return axios.get(ADMIN_USERS_BASE_URL + '/calorie',
        getOptions(token, { pseudo: pseudo }));
}
// UPDATE PHOTO

export const updatePhoto = (body, token) => {
    return axios.post(ADMIN_USERS_BASE_URL + '/photo', body, getOptions(token));
}
