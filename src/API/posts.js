import axios from "axios";
import { ADMIN_BASE_URL, getOptions } from './admin';

// POSTS

export const ADMIN_POSTS_BASE_URL = ADMIN_BASE_URL + '/posts';

// ENUM LIST

export const getAPIPostTypeList = (token) => {
    return axios.get(ADMIN_POSTS_BASE_URL + '/types', getOptions(token));
}

export const getAPIPostLanguageList = (token) => {
    return axios.get(ADMIN_POSTS_BASE_URL + '/languages', getOptions(token));
}



// CRUD POSTS

export const getAPIPostList = (params, token) => {
    return axios.get(ADMIN_POSTS_BASE_URL, getOptions(token, params));
}

export const getAPIPostDetail = (id, token) => {
    return axios.get(ADMIN_POSTS_BASE_URL + '/' + id, getOptions(token));
}

export const postAPIPost = (body, token) => {
    return axios.post(ADMIN_POSTS_BASE_URL, body, getOptions(token));
}

export const putAPIPost = (id, body, token) => {
    return axios.put(ADMIN_POSTS_BASE_URL + '/' + id, body, getOptions(token));
}

export const deleteAPIPost = (id, token) => {
    return axios.delete(ADMIN_POSTS_BASE_URL + '/' + id, getOptions(token));
}


// SCHEDULED POSTS

export const ADMIN_SCHEDULED_BASE_URL = ADMIN_BASE_URL + '/scheduled';

export const getAPIPostScheduleList = (token) => {
    return axios.get(ADMIN_SCHEDULED_BASE_URL, getOptions(token));
}

// POSTS IMAGES

export const addPhoto = (body, token) => {
    return axios.post(ADMIN_POSTS_BASE_URL + '/photo', body, getOptions(token));
}

