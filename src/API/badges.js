import axios from "axios";
import { ADMIN_BASE_URL, getOptions } from './admin';

export const ADMIN_BADGES_BASE_URL = ADMIN_BASE_URL + '/badges';

export const getAPIBadgeList = (token) => {
    return axios.get(ADMIN_BADGES_BASE_URL, getOptions(token));
};
