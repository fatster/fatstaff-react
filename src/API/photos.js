import axios from "axios";
import { ADMIN_BASE_URL, getOptions } from './admin';

export const ADMIN_PHOTO_BASE_URL = ADMIN_BASE_URL + '/photos'

//DELETE PHOTO
export const deletePhoto = (id, token) => {
    return axios.delete(ADMIN_PHOTO_BASE_URL + '/delete/' + id, getOptions(token))
}