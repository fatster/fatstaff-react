import axios from "axios";
import { ADMIN_BASE_URL, getOptions } from './admin';

const AUTH_BASE_URL = ADMIN_BASE_URL + '/auth';


// LOG IN AND RETURNS JWT COOKIE
export const postAPIAuth = (body) => {
    return axios.post(AUTH_BASE_URL, body);
};

// RETURN TRUE IF TOKEN IS STILL VALID
export const getAPIAuth = (token) => {
    return axios.get(AUTH_BASE_URL, getOptions(token));
}

// RETURN USER IF TOKEN IS VALID
export const getAPIAuthUser = () => {
    return axios.get(AUTH_BASE_URL + '/user');
}

