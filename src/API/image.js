export const BASE_URL = 'https://fatsterprod.imgix.net';


export const getFeedImage = (relativePath) => {
    return relativePath && 
    (
        relativePath.startsWith('feed/')
        || relativePath.startsWith('posts_default_images/')
    )
        ? BASE_URL + '/' + relativePath
        : '../img/no-image.jpg'
}
    ;

export const getProfileImage = (relativePath) => {
    return relativePath && relativePath.startsWith('profile/')
        ? BASE_URL + '/' + relativePath
        : '../img/no-profile.jpg'
}
    ;
