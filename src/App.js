import { useState } from "react";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import Menu from "./components/Navigation/Menu/menu";
import Messages from "./components/Navigation/Messages/messages";
import MessageSequence from "./components/Navigation/Messages/sequence";
import PrivateRoute from "./components/Navigation/PrivateRoute";
import LoginPage from "./pages/Login";
import PostsPage from "./pages/Posts";
import ProfilePage from "./pages/Profile";
import CaloriesPage from "./pages/Calories";
import UsersPage from "./pages/Users";


function App() {

  // USER & TOKEN MANAGEMENT

  const [profile, setProfile] = useState();


  // MESSAGES MANAGEMENT

  const [messages, setMessages] = useState([]);

  const setMessage = (message, actions) => {
    let newMessages = messages.slice();

    // empty messages list
    if (actions.includes('reset')) {
      newMessages = [];
    }

    // add new message to messages list
    if (actions.includes('add')) {
      message.id = MessageSequence.next();
      newMessages.push(message);

    // remove message from messages list
    } else if (actions.includes('remove')) {
      let index = newMessages.findIndex((elem) => elem.id === message.id);
      if (index > -1) {
        newMessages = messages.splice(index, 1);
      }
    }
    
    setMessages(newMessages);
  }


  // RENDERING

  return (
    <div>
      <BrowserRouter>

        <Menu profile={profile} setProfile={setProfile} setMessage={setMessage} />
        
        <Messages messages={messages} setMessage={setMessage} />

        <Switch>
          <Route exact path="/" >
            <LoginPage setProfile={setProfile} setMessage={setMessage} />
          </Route>

          {/* <PrivateRoute path="/stats" profile={profile} >
            <StatsPage profile={profile} setMessage={setMessage} />
          </PrivateRoute> */}

          <PrivateRoute path="/users/:id" profile={profile} >
            <UsersPage profile={profile} setMessage={setMessage} />
          </PrivateRoute>
          <PrivateRoute path="/users" profile={profile} >
            <UsersPage profile={profile} setMessage={setMessage} />
          </PrivateRoute>

          <PrivateRoute path="/posts/:pseudo" profile={profile} >
            <PostsPage profile={profile} setMessage={setMessage} />
          </PrivateRoute>
          <PrivateRoute path="/posts" profile={profile} >
            <PostsPage profile={profile} setMessage={setMessage} />
          </PrivateRoute>

          <PrivateRoute path="/calories/:pseudo" profile={profile} >
            <CaloriesPage profile={profile} setMessage={setMessage} />
          </PrivateRoute>
          <PrivateRoute path="/calories" profile={profile} >
            <CaloriesPage profile={profile} setMessage={setMessage} />
          </PrivateRoute>

          <PrivateRoute path="/profile" profile={profile} >
            <ProfilePage profile={profile} setProfile={setProfile} setMessage={setMessage} />
          </PrivateRoute>

          <Route><Redirect to="/" /></Route>
        </Switch>

      </BrowserRouter>
    </div>
  );

}

export default App;
